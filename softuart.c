/* softuart.c
 * robarago@gmail.com
 * Purpose: Software UART loop in SDCC with ASM delay and OSCCAL calibration
 * Copyright of main code by M.Saeed Yasin
 *   (http://saeedsolutions.blogspot.com.es, thanks!)
 * Copyright of tsmdelay.h
 *   (http://burningsmell.org, thanks!)
 */

#include <pic16regs.h>

#define KHZ 4000L
#include "tsmdelay.h"

// Config word
static __code unsigned int __at (_CONFIG) configword = _CP_OFF & _WDT_OFF & _BOREN_OFF & _INTRC_OSC_NOCLKOUT;
// Write "softuart v<major.minor>" in EEPROM for program identification
// --> to generate this line, select "softuart vX.X", press ":" and write "!str2bytes.py[ENTER]" in ex mode
// Warning: every program uploaded means an EEPROM write cycle, so don't do this every programming cycle!
static __code unsigned char __at 0x2100 __EEPROM[] = { 's', 'o', 'f', 't', 'u', 'a', 'r', 't', ' ', 'v', '3', '.', '0' };

#define Baudrate              1200        // bps
#define OneBitDelay						1000000/Baudrate
#define HalfBitDelay					1000000/Baudrate/2
#define DataBitCount          8           // no parity, no flow control
#define UART_TX               GP0					// UART TX pin
#define UART_RX               GP1					// UART RX pin
#define UART_TX_DIR			  TRISIO0					// UART TX pin direction register
#define UART_RX_DIR			  TRISIO1					// UART RX pin direction register

void osccal_init() {
	__asm
		call	0x3FF
		bsf		STATUS,5
		movwf	OSCCAL
	__endasm; 
}

void delay_half_bit() {
	DELAY_SMALL_US(HalfBitDelay);
}

void delay_one_bit() {
	DELAY_BIG_US(OneBitDelay);
}

void InitSoftUART(void) {
	UART_TX = 1;			// TX pin is high in idle state
	UART_RX_DIR = 1;		// Input
	UART_TX_DIR = 0;		// Output
}

unsigned char UART_Receive(void) {
	// Pin Configurations
  // GP1 is UART RX Pin
	unsigned char DataValue = 0;

	//wait for start bit
	while(UART_RX==1);

	delay_one_bit();  // Delay start bit
	delay_half_bit(); // Take sample value in the mid of bit duration

	for(unsigned char i = 0; i < DataBitCount; i++) {
		if(UART_RX == 1) {
			DataValue += (1<<i);
		}
		delay_one_bit();
	}

	// Check for stop bit
  // Stop bit should be high
	if(UART_RX == 1) {
		delay_half_bit();
		return DataValue;
	}
	else { //some error occurred !
		delay_half_bit();
		return 0x00;
	}
}

/* Basic Logic:
	 TX pin is usually high. A high to low bit is the starting bit and 
	 a low to high bit is the ending bit. No parity bit. No flow control.
	 BitCount is the number of bits to transmit. Data is transmitted LSB first.
*/
void UART_Transmit(const char DataValue) {
	// Send Start Bit
	UART_TX = 0;
	delay_one_bit();

	for(unsigned char i = 0; i < DataBitCount; i++) {
		//Set Data pin according to the DataValue
		if(((DataValue>>i)&0x1) == 0x1) { //if Bit is high
			UART_TX = 1;
		}
		else { //if Bit is low
			UART_TX = 0;
		}
    delay_one_bit();
	}

	//Send Stop Bit
	UART_TX = 1;
	delay_one_bit();
}

// Main function
void main() {	
	unsigned char ch0, ch1;

	osccal_init();

	ANSEL  = 0x00;     // Set ports as digital I/O, not analog input
	INTCON = 0x00;     // Enable interrupts for timer0
	ADCON0 = 0x00;		 // Shut off the A/D Converter
	CMCON  = 0x07;		 // Shut off the Comparator
	VRCON  = 0x00;	   // Shut off the Voltage Reference
	GPIO   = 0x00;     // Make all pins 0
	TRISIO = 0x00;     // GP3 input, rest all output

	InitSoftUART();		 // Intialize Soft UART

	while(1) {
		ch1 = 0;
		ch0 = UART_Receive();	// Receive a character from UART
		if(ch0 == 0xC2 || ch0 == 0xC3) ch1 = UART_Receive();
		UART_Transmit(ch0);		// Echo back that character
		if(ch1) UART_Transmit(ch1);		// Echo back that character
	}
}
